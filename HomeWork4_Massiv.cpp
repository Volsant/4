﻿#include <time.h>
#include <iostream>
int Day()
{
    struct tm buf;
    time_t t = time(NULL);
    localtime_s(&buf, &t);
    int day = buf.tm_mday;   // Создание переменной с текущей датой
    return day;
}

int main()
{        
    const int size = 5;
    int array[size][size];    // Создание массива
    
    int r = 0;
    int Line; 
    int indexday = Day() % size;  // Остаток деления текущей даты на размер массива
    
    for (int i = 0; i < size; ++i)    //Создание массива и вывод в консоль
    {           
        std::cout << "Line - " << i << ":  ";

        for (int j = 0; j < size; ++j)
        {
            array[i][j] = j + i;
            std::cout << array[i][j];

            if (indexday == i)
            {
                Line = i;

                r = r + array[i][j];                             
            }
        }                          
    std::cout << "\n";
    }
        
    std::cout << "\n" << "\Date: " << Day() << "\nCurrent Line: " << Line << "\nSumma: " << r;
            
}

